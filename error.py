class Error(Exception):
    """Base class for other exceptions"""
    pass
class NoMatchingItemsError(Error):
    """Raised when no items match"""
    pass
