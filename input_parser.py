import argparse
import json

class InputParser:
    def __init__(self):
        parser = argparse.ArgumentParser(description='Process some Shopping Cart Items.')
        parser.add_argument('args', nargs='*')
        self.user_input = parser.parse_args().args

    def items(self):
        items = []
        for item in self.user_input:
            items.append(item)
        return items