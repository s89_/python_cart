import json

class Offers:
    def __init__(self):
        self.json_offers = './data/offers.json'
        self.offers = []

    def load_offers(self):
        with open(self.json_offers) as json_offers:
            offers = json.load(json_offers)
            for offer in offers:
                self.offers.append(offer)