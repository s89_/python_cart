from item import Item
import json

class Inventory:
    def __init__(self):
        self.stock_items = []
        self.json_items = './data/inventory.json'

    def load_items(self):
        with open(self.json_items) as json_items:
            items = json.load(json_items)
            for item in items:
                stock_item = Item(item, items[item])
                self.stock_items.append(stock_item)