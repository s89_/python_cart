# Python ShoppingCart

A basic Shopping Cart implementation in python.

## Getting Started

The Shopping Cart can be run as follows

```
./ShoppingCart Onions Bread Milk
```

You can add Items to your cart in the form of String arguments when you run the program.

### Prerequisites

You will need to have Python 3 installed to run this program.

### Loading / Changing Data

The program loads data from the /data directory.  In there, you will find JSON data for the Inventory of Items, and Offers.

If you wish to add any offers, this will require adjustments to the code.  Items, however, can be added in without any code changes.

## Running the tests

Run the tests with the `pytest` command, from the root folder.