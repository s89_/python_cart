from item import Item
from offers import Offers
from inventory import Inventory
from error import NoMatchingItemsError

import math

class ShoppingCart:
    def __init__(self, inventory, item_names):
        self.items = []
        self.item_names = item_names
        self.offers = []
        self.offer_items = []
        self.inventory_items = inventory.stock_items
        self.total = 0
        self.discounted_total = 0

    def add_items(self):
        for item in self.item_names:
            self.add_item(item)

    def add_item(self, name):
        try:
            item_not_found = True
            for item in self.inventory_items:
                if item.name == name:
                    item_not_found = False
                    self.items.append(item)
                    self.total += float(item.price)
            if item_not_found:
                raise NoMatchingItemsError(item)
        except NoMatchingItemsError:
            print((f'NoMatchingItemsError: {name} is not in the inventory.  Please try another input.'))

    def parsed_total(self):
        return f"£{'%.2f' % round(self.total, 2)}"

    def parsed_discounted_total(self):
        return f"£{'%.2f' % round(self.discounted_total, 2)}"

    def load_offers(self):
        offers = Offers()
        offers.load_offers()
        self.offers = offers.offers

    def apply_offers(self):
        self.discounted_total = 0
        for offer in self.offers:
            if 'Buy one get one half price' in offer:
                self.offer_items.append(offer['Buy one get one half price']['Item'])
                self.buy_one_get_one_half_price(offer['Buy one get one half price']['Item'])
            if '10 percent off' in offer:
                self.offer_items.append(offer['10 percent off']['Item'])
                self.ten_percent_off(offer['10 percent off']['Item'])
        self.calculate_discounted_total()

    def buy_one_get_one_half_price(self, discount_item):
        discounted_items = math.floor(self.item_names.count(discount_item) / 2)
        count = 0
        for item in self.items:
            if item.name == discount_item and count < discounted_items and discounted_items >= 1:
                self.discounted_total += (float(item.price) / 2)
                count += 1
            elif item.name == discount_item:
                self.discounted_total += float(item.price)

    def ten_percent_off(self, discount_item):
        for item in self.items:
            if item.name == discount_item:
                self.discounted_total += (float(item.price) * 0.90)

    def calculate_discounted_total(self):
        for item in self.items:
            if item.name not in self.offer_items:
                self.discounted_total += float(item.price)

    def discount_amount(self):
        if round(self.total, 2) != round(self.discounted_total, 2):
            return round(self.total - self.discounted_total, 2)
        else:
            return 'none'