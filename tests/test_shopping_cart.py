from ..inventory import Inventory
from ..shopping_cart import ShoppingCart

inventory = Inventory()
cart = ShoppingCart(inventory, ['Bread', 'Onions', 'Beans', 'Beans', 'Milk'])
inventory.load_items()
cart.add_items()

def test_add_items():
    assert len(cart.items) == 5

def test_total():
    assert round(cart.total, 2) == 4.55

def test_parsed_total():
    assert cart.parsed_total() == "£4.55"

def test_load_offers():
    cart.load_offers()
    assert len(cart.offers) == 2

def test_discounted_total():
    cart.apply_offers()
    assert round(cart.discounted_total, 2) == 4.09

def test_parsed_discounted_total():
    assert cart.parsed_discounted_total() == "£4.09"

def test_discount_amount():
    assert cart.discount_amount() == 0.46