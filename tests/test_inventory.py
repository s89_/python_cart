from ..inventory import Inventory

inventory = Inventory()

def test_load_items():
    inventory.load_items()
    assert len(inventory.stock_items) == 4