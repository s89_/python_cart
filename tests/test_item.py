from ..item import Item

onion = Item('Bread', 0.85)

def test_item_price():
    assert onion.price == 0.85

def test_item_name():
    assert onion.name == 'Bread'