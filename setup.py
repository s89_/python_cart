from setuptools import setup, find_packages

setup(
    name='Aker Shopping Cart',
    version='0.1',
    description='A basic Shopping Cart',
    author='Scott Murray',
    author_email='sm@smrry.com',
    url='https://github.com/smrr723',
    packages=find_packages(exclude=('tests'))
)